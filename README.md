# Password manager

A simple password manager app build with Ionic framework.

The application supports finger verification.  
The hashing algorythm can be manipulated via settings.

**This is a simple project aimed to learn Ionic framwework**

An APK can be downloaded from [here](https://gitlab.com/neoprog.biz/ionic-password-manager/blob/master/platforms/android/app/build/outputs/apk/debug/app-debug.apk).