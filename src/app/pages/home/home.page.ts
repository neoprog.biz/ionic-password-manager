import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { ModalController, AlertController, Platform, ActionSheetController } from '@ionic/angular';
import { FormComponent } from '../../components/form/form.component';
import { DatabaseService } from '../../services/database.service';
import { ToasterService } from '../../services/toaster.service';
import { HasherService } from '../../services/hasher.service';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { SessionService } from 'src/app/services/session.service';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy, AfterViewInit {
    /**
     * @var Array list of passwords data objects
     */
    passwords: Array<Object>;
    /**
     * @var Subscriber of a back button press event
     */
    backBtnSubscr;
    appPauseSubscr;

    constructor(
        private modalController: ModalController,
        private alertController: AlertController,
        private database: DatabaseService,
        private toaster: ToasterService,
        private hasher: HasherService,
        private clipboard: Clipboard,
        private session: SessionService,
        private platform: Platform,
        private actionsCtrl: ActionSheetController
    ) { }

    ngOnInit() {
        this.session.login().then(success => {
            if (!success) {
                return this.toaster.error("Login failed");
            }

            this._refreshList();
        });
    }

    ngAfterViewInit() {
        this.backBtnSubscr = this.platform.backButton.subscribe(() => {
            navigator["app"].exitApp();
        });

        this.appPauseSubscr = this.platform.pause.subscribe(() => {
            this.database.clearUserToken();
        });
    }

    ngOnDestroy() {
        if (this.backBtnSubscr) {
            this.backBtnSubscr.unsubscribe();
        }

        if (this.appPauseSubscr) {
            this.appPauseSubscr.unsubscribe();
        }
    }
    /**
     * Reload passwords list from storage
     */
    _refreshList() {
        this.database.getPasswords().then(result => {
            this.passwords = result;
        });
    }
    /**
     * Display password details for the given list entry
     * 
     * @param int num index of the selected list entry
     * 
     * @return Promise
     */
    async showPassword(num: number) {
        let options = {
            header: "Password details",
            message: "No details found - invalid item index",
            buttons: []
        }


        if (num >= 0) {
            let pass = await this.database.getPassword(num);
            options.message = `<ion-item class="ion-no-padding strings-break">
                    ${pass["login"]}@${pass["host"]}
                </ion-item>
                <ion-item class="ion-no-padding strings-break">
                    ${pass["password"]}
                </ion-item>`;

            options.buttons.push(
                {
                    text: 'Copy',
                    icon: 'download',
                    handler: () => {
                        this.copyPasswordToClipboard(pass["password"]);
                    }
                }
            );
        }

        options.buttons.push("Ok");

        const alert = await this.alertController.create(options);

        await alert.present();
    }
    /**
     * Programmically copy password to clipboard
     * 
     * @param string pass password string
     */
    copyPasswordToClipboard(pass: string) {
        if (!pass || pass.length <= 0) {
            return this.toaster.error("No password text to copy!");
        }

        return this.clipboard.copy(pass);
    }
    /**
     * Display modal with new password form
     */
    async showForm() {
        const modal = await this.modalController.create(
            {
                component: FormComponent,
                cssClass: "password-form-modal"
            }
        );

        modal.onWillDismiss().then(params => {
            if (!params || !params.data || !params.data.host || !params.data.login) {
                if (typeof (params.data) !== "undefined"
                    && (params.data.hasOwnProperty("host") || params.data.hasOwnProperty("login"))
                ) {
                    this.toaster.error("Can't add password - form was empty");
                }

                return;
            }

            let newEntry = params.data;
            newEntry.index = (new Date()).getTime();
            // @NOTE use service to generate password
            this.hasher.generatePassword(newEntry.host, newEntry.login).then(password => {
                newEntry.password = password;

                this.database.savePassword(newEntry).then(() => {
                    this.toaster.success("Password added");
                    this._refreshList();
                }).catch(reason => {
                    console.log(reason);
                    this.toaster.error("Failed to save password");
                });
            });

        });

        await modal.present();
    }

    removePassword(index: number) {
        if (!index || index <= 0) {
            this.toaster.error("Invalid item index");
            return false;
        }

        this.toaster.confirmation(
            "Delete this item?",
            () => {
                this.database.removePassword(index).then(check => {
                    if (check) {
                        this.toaster.success("Element removed");
                        this._refreshList();
                    }
                })
            }
        );
    }

    get passwordsIterator(): Array<any> {
        return Object.keys(this.passwords);
    }

    async showItemActionMenu(index: number) {
        if (!index || index <= 0) {
            this.toaster.error("Invalid item index");
            return false;
        }

        let pass = await this.database.getPassword(index);

        const menu = await this.actionsCtrl.create(
            {
                header: `${pass["login"]}@${pass["host"]}`,
                buttons: [
                    {
                        text: 'Delete',
                        role: 'destructive',
                        icon: 'trash',
                        handler: () => {
                            this.removePassword(index);
                        }
                    }, {
                        text: 'Copy',
                        icon: 'download',
                        handler: () => {
                            this.copyPasswordToClipboard(pass["password"]);
                        }
                    }, {
                        text: 'View',
                        icon: 'eye',
                        handler: () => {
                            this.showPassword(index);
                        }
                    }, {
                        text: 'Cancel',
                        icon: 'close',
                        role: 'cancel'
                    }
                ]
            }
        );

        await menu.present();
    }
}
