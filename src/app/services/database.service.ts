import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

const STORAGE_KEY  = "passesInfo";
const SETTINGS_KEY = "appSettings";
const SESSION_KEY  = "userToken";

@Injectable({
    providedIn: 'root'
})
export class DatabaseService {

    constructor(private storage: Storage) { }

    getSettings() {
        return this.storage.get(SETTINGS_KEY);
    }

    getPasswords() {
        return this.storage.get(STORAGE_KEY);
    }

    async savePassword(passData: Object): Promise<null> {
        let passwords = await this.getPasswords();

        if (!passwords) {
            passwords = {};
        }

        passwords[passData["index"]] = passData;

        return this.storage.set(STORAGE_KEY, passwords);
    }

    saveSettings(settings: Object) {
        return this.storage.set(SETTINGS_KEY, settings);
    }

    async getPassword(index: number): Promise<null|Object> {
        if (!index || index <= 0) {
            return null;
        }

        let passwords = await this.getPasswords();

        if (!passwords || !passwords.hasOwnProperty(index)) {
            return null;
        }

        return passwords[index];
    }

    async removePassword(index: number): Promise<boolean> {
        if (!index || index <= 0) {
            return false;
        }

        let passwords = await this.getPasswords();

        if (!passwords || !passwords.hasOwnProperty(index)) {
            return false;
        }

        delete passwords[index];

        return this.storage.set(STORAGE_KEY, passwords);
    }

    updateUserToken(token: Object) {
        return this.storage.set(SESSION_KEY, token);
    }

    getUserToken(): Promise<null | Object> {
        return this.storage.get(SESSION_KEY);
    }

    clearUserToken() {
        return this.storage.remove(SESSION_KEY);
    }
}
