import { Injectable } from '@angular/core';
import { DatabaseService } from './database.service';
import { ToasterService } from './toaster.service';

@Injectable({
    providedIn: 'root'
})
export class HasherService {

    constructor(private database: DatabaseService, private toaster: ToasterService) { }

    async generatePassword(host: string, login: string): Promise<string> {
        const removable = ["https://", "http://", "www."];
        let settings    = await this.database.getSettings();

        if (!settings || !settings.salt) {
            this.toaster.error("Can't generate password without 'salt' setting");
            return Promise.reject();
        }

        let algorithmParts = {
            salt: settings.salt,
            login: login
        };

        removable.forEach(element => {
            if (host.includes(element)) {
                host = host.replace(element, "");
            }
        });

        algorithmParts["hostParts"] = host.split(".");

        let password = "";

        settings.algorithm.forEach((element: string) => {
            if (algorithmParts[element]) {
                password += algorithmParts[element];
            } else if (element.includes("-")) {
                let indexes = element.split("-");

                password += algorithmParts[indexes[0]][indexes[1]];
            }
        });
        
        return password;
    }
}
