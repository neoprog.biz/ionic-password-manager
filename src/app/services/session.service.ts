import { Injectable } from '@angular/core';
import { generate as randomStringGenerator } from 'stringing';
import { DatabaseService } from './database.service';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { Platform } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class SessionService {
    constructor(
        private database: DatabaseService,
        private fingerPrt: FingerprintAIO,
        private platform: Platform
    ) { }

    async login(): Promise<boolean> {
        if (await this.isLoggedIn()) {
            return true;
        }

        try {
            const platforms = this.platform.platforms();
            // @NOTE makes desctop version without cordova fully functional but not secure
            if (platforms.indexOf("desktop") > -1) {
                return true;
            }

            const available = await this.fingerPrt.isAvailable();
            // @NOTE won't work for devices without fingerprint scanner
            if (available !== "finger") {
                return false;
            }
            
            const result = await this.fingerPrt.show({
                clientId: 'PasswordMgrVerFinger',
                clientSecret: randomStringGenerator(20),
                disableBackup: false
            });

            let tmpDate = new Date();
            tmpDate.setMinutes(tmpDate.getMinutes() + 30);
            result.timestamp = tmpDate.getTime();

            this.database.updateUserToken(result);

            return true;
        }
        catch (error) {
            console.error(error);
            return false;
        }
    }

    async isLoggedIn(): Promise<boolean> {
        const token = await this.database.getUserToken();

        if (!token || !token["timestamp"] || token["timestamp"] < Date.now()) {
            return false;
        }

        return true;
    }
}
