import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class ToasterService {

    constructor(private toastCtrl: ToastController) { }

    async success(message: string) {
        if (!message || message.length <= 0) {
            console.log("Toast error: no message provided");
            
            return false;
        }

        let toast = await this.toastCtrl.create(
            {
                header: "Success!",
                message: message,
                position: "bottom",
                duration: 2000
            }
        );

        toast.present();
    }

    async error(message: string) {
        if (!message || message.length <= 0) {
            console.log("Toast error: no message provided");

            return false;
        }

        let toast = await this.toastCtrl.create(
            {
                header: "Error!",
                message: message,
                position: "bottom",
                buttons: [
                    {
                        side: "end",
                        text: "Ok"
                    }
                ]
            }
        );

        toast.present();
    }

    async confirmation(message: string, callback: Function) {
        if (!message || message.length <= 0) {
            console.log("Toast error: no message provided");

            return false;
        }
        
        let toast = await this.toastCtrl.create(
            {
                header: "Please confirm:",
                message: message,
                position: "middle",
                cssClass: "confirmation-box",
                buttons: [
                    {
                        side: "start",
                        text: "Yes",
                        handler: () => { callback(); }
                    },
                    {
                        side: "end",
                        text: "No"
                    }
                ]
            }
        );

        toast.present();
    }
}
